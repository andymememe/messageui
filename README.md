# MessageUI #
### MessageVo ###
MessageVo是訊息內容。

屬性        | 說明
:---------:|:----------:
content    | 訊息內容物
direction  | 訊息傳輸方

### MessageAdapter ###
屬性         | 說明
:-----------:|:----------:
messageVo    | 訊息列表

notifyDataSetChanged : 當新增訊息時,呼叫此函數會更新界面

### Example ###

```java
private List<MessageVo> meList = new ArrayList<MessageVo>();
private MessageAdapter messageAdapter = new MessageAdapter(this, meList);

MessageVo message = new MessageVo(MessageVo.MESSAGE_FROM, "I'm content.");
meList.add(message);
messageAdapter.notifyDataSetChanged();
```
