package com.example.artificialintelligence;

public class MessageVo {
     public static final int MESSAGE_FROM = 0;
     public static final int MESSAGE_TO = 1;

     private int direction;
     private String content;

     public MessageVo(int direction, String content) {
         super();
         this.direction = direction;
         this.content = content;

     }

     public MessageVo(int direction, String content) {
         super();
         this.direction = direction;
         this.content = content;

     }

     public int getDirection() {
         return direction;
     }

     public String getContent() {
         return content;
     }

     public void setDirection(int direction) {
         this.direction = direction;
     }
     
     public void setContent(String content) {
         this.content = content;
     }

 }
