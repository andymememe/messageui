package com.example.artificialintelligence;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

 public class MessageAdapter extends BaseAdapter{
     protected static final String TAG = "MessageAdapter";
     private Context context;
     private List<MessageVo> messageVo;


     public MessageAdapter(Context context, List<MessageVo> messageVo) {
         super();
         this.context = context;
         this.messageVo = messageVo;
     }


     @Override
     public int getCount() {
         // TODO Auto-generated method stub
         return messageVo.size();
     }


     @Override
     public Object getItem(int position) {
         // TODO Auto-generated method stub
         return messageVo.get(position);
     }


     @Override
     public long getItemId(int position) {
         // TODO Auto-generated method stub
         return position;
     }


     @SuppressLint("InflateParams")
	@Override
     public View getView(int position, View convertView, ViewGroup parent) {
         ViewHolder holder = null;
         MessageVo message = messageVo.get(position);

         if(convertView == null || (holder = (ViewHolder)convertView.getTag()).flag != message.getDirection())
         {
             holder = new ViewHolder();
             if(message.getDirection() == MessageVo.MESSAGE_FROM)
             {
                 holder.flag = MessageVo.MESSAGE_FROM;
                 convertView = LayoutInflater.from(context).inflate(R.layout.item_from, null);

             }
             else
             {
                 holder.flag = MessageVo.MESSAGE_TO;
                 convertView = LayoutInflater.from(context).inflate(R.layout.item_to, null);
             }

             holder.content = (TextView)convertView.findViewById(R.id.content);
             convertView.setTag(holder);
         }
         holder.content.setText(message.getContent());

         return convertView;
     }

     protected void startActivity(Intent it) {
		// TODO Auto-generated method stub
	}

	static class ViewHolder
     {
         int flag;
         TextView content;
         TextView time;
     }

 }
